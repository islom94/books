<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 15)->create()->each(function($book) {
            $book->authors()->saveMany(factory(App\Author::class, mt_rand(1, 3))->make());
        });
    }
}
