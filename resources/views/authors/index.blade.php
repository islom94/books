@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Authors
                </span>
                <a class="btn btn-primary float-right" href="{{ route('authors.create') }}" role="button">Add</a>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                        @foreach ($authors as $author)
                            <tr>
                                <td>{{ $author->name }}</td>
                                <td>{{ $author->books_count }}</td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group mr-2" role="group">
                                            <a class="btn btn-secondary" href="{{ route('authors.edit', $author->id) }}">Edit</a>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="First group">
                                            <form method="POST" action="{{ route('authors.destroy', $author->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $authors->links() }}
    </div>
@endsection
