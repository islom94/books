@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Authors
                </span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('authors.update', $author->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="name" name="name" class="form-control" id="name" value="{{ $author->name }}" placeholder="Enter name">
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
@endsection
