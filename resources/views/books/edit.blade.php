@extends('layouts.app')
@push('scripts')
<script>
    window.addEventListener('load', function() {
        $('#authors').selectpicker();
    });
</script>
@endpush
@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Books
                </span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('books.update', $book->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="name" name="name" class="form-control" id="name" value="{{ $book->name }}" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="name" name="price" class="form-control" id="price" value="{{ $book->price }}" placeholder="Enter price">
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" name="date" class="form-control" value="{{ $book->date }}" id="date">
                    </div>
                    <div class="form-group mt-3">
                        <label for="authors">Authors</label>
                        <select name="authors[]" id="authors" class="selectpicker form-control" multiple data-live-search="true">
                            @foreach ($authors as $author)
                                <option value="{{ $author->id }}" @if ($book->authors->contains($author->id)) selected @endif>{{ $author->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
@endsection
