@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <div class="card">
            <div class="card-header">
                <span>
                    Books
                </span>
                <a class="btn btn-primary float-right" href="{{ route('books.create') }}" role="button">Add</a>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                        @foreach ($books as $book)
                            <tr>
                                <td>
                                   <b> {{ $book->name }} <b>
                                </td>
                                <td>
                                    {{ $book->price }}
                                </td>
                                <td>
                                    {{ $book->date }}
                                </td>
                                <td>
                                    @if ($book->authors()->exists())
                                        <b>Authors</b><br>
                                        <ol>
                                            @foreach ($book->authors as $author)
                                                <li>
                                                    {{ $author->name }}
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group mr-2" role="group">
                                            <a class="btn btn-secondary" href="{{ route('books.edit', $book->id) }}">Edit</a>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="First group">
                                            <form method="POST" action="{{ route('books.destroy', $book->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $books->links() }}
    </div>
@endsection
